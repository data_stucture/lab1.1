public class RemoveDuplicates {
    public static void main(String[] args) {
        int[] nums = {0, 0, 1, 1, 1, 2 ,2 ,3, 3, 4};
        System.out.println("Original Array");
        for (int num : nums) System.out.print(num + " ");
        System.out.println();
        System.out.println("Array after removing duplicates");
        int k = removeDuplicates(nums);
        for (int i = 0; i < k; i++) System.out.print(nums[i] + " ");
    }

    private static int removeDuplicates(int[] nums) {
        int k = 0;
        for (int i = 0; i < nums.length; i++) {
            if (i < nums.length - 1 && nums[i] == nums[i + 1]) {
                continue;
            }
            nums[k] = nums[i];
            k++;
        }
        return k;
    }
}
