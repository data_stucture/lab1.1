public class SearchinRotatedSortedArray {
    public static int search(int[] nums, int target) {
        int left = 0;
        int right = nums.length-1;
        int mid;
        while (left <= right) {
            mid = left + (right - left)/2;
            if (nums[mid] == target) return mid;
            if (nums[mid] >= nums[left]) {
                if (target >= nums[left] && target < nums[mid]) right = mid-1;
                else left = mid+1;
            } else {
                if (target <= nums[right] && target > nums[mid]) left = mid+1;
                else right = mid-1;
            }
        }
        return -1;

    }
    public static void main(String[] args) {
        int[] nums = {4,5,6,7,0,1,2};
        int target = 3;
        int answer = search(nums, target);
        System.out.println("The index of " + target + " is " + answer);
        
    }
}
