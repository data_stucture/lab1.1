import java.util.Arrays;

public class ArrayManipulation {
    public static void main(String[] args) {
        int[] numbers = {5, 8, 3, 2, 7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];
        int sum = 0;
        double max = 0;
        System.out.print("3. Print the elements of the \"numbers\" array using a for loop :");
        for (int i = 0; i<numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }
        System.out.println();
        System.out.print("4. Print the elements of the \"names\" array using a for-each loop. :");
        for (String name : names) {
            System.out.print(name + " ");
        }
        // 5. Initialize the elements of the "values" array with any four decimal values of your choice.
        values[0] = 651.6021;
        values[1] = 652.6221;
        values[2] = 661.6421;
        values[3] = 666.6666;
        System.out.println("Lab 1.1");
        System.out.print("6. Calculate and print the sum of all elements in the \"numbers\" array. :");
        for (int num : numbers) {
            sum += num;
        }
        System.out.print(sum);

        System.out.print("7. Find and print the maximum value in the \"values\" array. :");
        for (double num : values) {
            if (num > max) max = num;
        }
        System.out.println(max);

        System.out.print("8. Create a new string array named \"reversedNames\" with the same length as the \"names\" array. :");
        String[] reversedNames = new String[names.length];
        for (int i = 0; i < names.length; i++) {
            reversedNames[i] = names[names.length - i - 1];
        }
        for (String name : reversedNames) {
            System.out.print(name + " ");
        }
        System.out.println();
        System.out.print("9. BONUS: Sort the \"numbers\" array in ascending order using any sorting algorithm of your choice. :");
        Arrays.sort(numbers);
        for (int num : numbers) {
            System.out.print(num + " ");
        }
    }
}
