import java.util.Scanner;

public class DuplicateZeros {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] num = new int[n];
        for (int i = 0; i < num.length; i++) num[i] = sc.nextInt();
        System.out.println("Original Array");
        for (int m : num) System.out.print(m + " ");
        System.out.println();
        System.out.println("Duplicate Zeros");
        shifRight(num);
    }

    public static void shifRight(int[] arr) {
        int index = 0;
        for (int i : arr) if (i == 0) index++;
        for (int i = arr.length - 1, j = arr.length + index - 1; i < j; --i, --j) {
            if (j < arr.length)
                arr[j] = arr[i];
            if (arr[i] == 0)
                if (--j < arr.length)
                    arr[j] = arr[i];
        }
        for (int m : arr) System.out.print(m + " ");
    }
}
