public class Sqrt {
    public static void main(String[] args) {
        int x = 8;
        int answer = mySqrt(x);
        System.out.println("The square root of " + x + " is " + answer);
    }

    public static int mySqrt(int x) {
        if (x < 0 ) return -1;
        if (x == 0) return 0;
        if (x == 1) return 1;
        int left = 1;
        int right = x;
        int mid, sqrt;
        while (left <= right) {
            mid =  left + (right - left)/ 2;
            sqrt = x / mid;
            if (sqrt == mid) return mid;
            else if (sqrt < mid) right = mid - 1;
            else left = mid + 1;
        }
        return right;

    }
}
